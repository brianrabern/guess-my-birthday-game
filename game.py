from random import randint

name = input("Hi! What is your name? ")
guesses = [1, 2, 3, 4, 5, 6]

for guess in guesses:
    if guess == 6:
        print("I have other things to do. Good bye.")
        break

    year_guess = randint(1924, 2004)
    month_guess = randint(1, 12)

    print(f"Guess {guess}: {name} were you born in {month_guess}/{year_guess}?")
    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        break

    if response == "no" and guess < 5:
        print("Drat! Lemme try again!\n")
