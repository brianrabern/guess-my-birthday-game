from random import randint

name = input("\nHi! What is your name? ")
guesses = [1, 2, 3, 4, 5, 6]

for guess in guesses:
    if guess == 6:
        print("\nOk. I have other things to do. Later.\n")
        break

    year_guess = randint(1924, 2004)
    month_guess = randint(1, 12)

    print(
        f"\nGuess {guess}: {name} were you born in {month_guess}/{year_guess}?")
    response = input("yes or no? ")

    if str.lower(response) == "yes" or str.lower(response) == "y":
        print("I knew it!\n\nBye!\n")
        break

    if (str.lower(response) == "no" or str.lower(response) == "n") and guess < 5:
        print("Drat! Lemme try again!")
